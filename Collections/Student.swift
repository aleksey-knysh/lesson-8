//
//  Student.swift
//  Collections
//
//  Created by Aleksey Knysh on 1/26/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation
struct Student {
    var name: String
    var surname: String
    var patronymic: String
    var yearOfBirth: Int
}

extension Student: Hashable {
    static func == (lhs: Student, rhs: Student) -> Bool {
        lhs.name == rhs.name && lhs.surname == rhs.surname && lhs.patronymic == rhs.patronymic && lhs.yearOfBirth == rhs.yearOfBirth
    }
}
