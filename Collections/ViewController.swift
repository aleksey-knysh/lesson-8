//
//  ViewController.swift
//  Collections
//
//  Created by Aleksey Knysh on 1/16/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // ложим 10 рандомных чисел от 1-50 в массив
        var arrayOfNumbers = [Int]()
        for _ in 0...10 {
            let range = Int.random(in: 1...50)
            arrayOfNumbers.append(range)
        }
        print("\nМассив рандомных чисел - \(arrayOfNumbers)\n\n")
        
        // coздаём новый массив с числами из первого, которые >30
        var copyOfArrayNumbers = [Int]()
        let filterNumber = arrayOfNumbers.filter { (value) -> Bool in value > 30 }
        copyOfArrayNumbers.append(contentsOf: filterNumber)
        print("Числа которые >30 из первого массива - \(copyOfArrayNumbers)\n\n")
        
        // новый массив строк из первого массива чисел
        let arrayString = arrayOfNumbers.flatMap { String($0) }
        print("Массив строк из первого массива\n\(arrayString)\n\n")
        
        // выводим сумму элементов первого массива
        let sumElementArrayOfNumders = arrayOfNumbers.reduce(0, +)
        print("Выводим среднее арифмитическое элементов первого массива - \(sumElementArrayOfNumders)\n\n")
        
        // новый массив, который содержит чисел ниже среднего арифмитического числа из первого массива
        let numberOfElementsInArray = arrayOfNumbers.count
        let averageArrayOfNumbers = sumElementArrayOfNumders / numberOfElementsInArray
        let filteringBelowAverage = arrayOfNumbers.filter { (value) -> Bool in value < averageArrayOfNumbers }
        var newArrayAverage = [Int]()
        newArrayAverage.append(contentsOf: filteringBelowAverage)
        print("среднее арифмитическое - \(averageArrayOfNumbers) - массив чисел ниже среднего арфм. первого массива - \(filteringBelowAverage)\n\n")
        
        // создаём словарь месяцев с использованием кортежей
        let dictionary =
            ["Январь": (31, 1),
             "Февраль": (28, 2),
             "Март": (31, 3),
             "Апрель": (30, 4),
             "Май": (31, 5),
             "Июнь": (30, 6),
             "Июль": (31, 7),
             "Август": (31, 8),
             "Сентябрь": (30, 9),
             "Октябрь": (31, 10),
             "Ноябрь": (30, 11),
             "Декабрь": (31, 12)]
        
        // Выводим название месяца и количество дней
        var sumDays = 0
        print("Выводим словарь месяцев\n-----------------------")
        for (keys, value) in dictionary {
            print("Месяц - \(keys)\nКоличество дней - \(value.0)\n")
            
            // выводим сумму дней в году
            sumDays += value.0
        }
        print("\nСумма дней в году - \(sumDays)\n\n")
        
        // словарь пор года
        let dictionaryTimeOfYear = ["Зима": ["Декабрь, Январь, Февраль"],
                                    "Весна": ["Март, Апрель, Май"],
                                    "Лето": ["Июнь, Июль, Август"],
                                    "Осень": ["Сентябрь, Октябрь, Ноябрь"]]
        
        print("Словарь пор года\n----------------")
        for (season, month) in dictionaryTimeOfYear {
            print("\(season), \(month)")
        }
        
        // Множество чётных и нечётных чисел
        let evenNumbers = arrayOfNumbers.filter({ $0 % 2 == 0 })
        let setEvenNumbers = Set(evenNumbers)
        print("\nСет чётных чисел \(setEvenNumbers)")
        
        let oddNumbers = arrayOfNumbers.filter({ $0 % 2 != 0 })
        let setOddNumbers = Set(oddNumbers)
        print("\nСет нечётных чисел \(setOddNumbers)")
        
        // является ли первое множество подмножество второго и наоборот
        print("\nЯвляется ли setEvenNumbers подмножеством setOddNumbers - \(setEvenNumbers.isSubset(of: setOddNumbers))\n\nЯвляется ли setOddNumbers подмножеством  setEvenNumbers - \(setOddNumbers.isSubset(of: setEvenNumbers))\n")
        
    // создаём Struct Student, инициализируем и помещаем её значения в Set
        
        let student = [Student(name: "Игорь", surname: "Игоревич", patronymic: "Игорёк", yearOfBirth: 1995)]
        let student1 = [Student(name: "Рома", surname: "Романов", patronymic: "Романович", yearOfBirth: 1996)]
        let student2 = [Student(name: "Илья", surname: "Ильин", patronymic: "Ильюхавич", yearOfBirth: 1997)]
        let student3 = [Student(name: "Вася", surname: "Васинька", patronymic: "Васюнин", yearOfBirth: 1998)]
        
        let setStudent: Set = [student, student1, student2, student3]
        print(setStudent)
    }
}
